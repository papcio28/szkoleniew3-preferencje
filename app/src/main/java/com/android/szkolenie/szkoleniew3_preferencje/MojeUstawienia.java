package com.android.szkolenie.szkoleniew3_preferencje;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by papcio28 on 10.05.2015.
 */
public class MojeUstawienia {
    private static final String PREFS_NAME = "moje_ustawienia";

    private static final String KEY_TEXT = "pref.text";
    private static final String KEY_CHECK = "pref.check";
    private static final String KEY_POSITION = "pref.position";

    private String mText; // Preferencja TEXT
    private Boolean mCheck; // Preferencja CHECK
    private Integer mPosition; // Preferencja POSITION

    private SharedPreferences mPreferences;

    public MojeUstawienia(Context mContext) {
        // Pobranie obiektu preferencji z systemu
        mPreferences = mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        // Odświeżenie wartości przy tworzeniu obiektu
        refresh();
    }

    public MojeUstawienia refresh() {
        // Aktualizacja wartości z SharedPreferences
        this.mText = mPreferences.getString(KEY_TEXT, "");
        this.mCheck = mPreferences.getBoolean(KEY_CHECK, false);
        this.mPosition = mPreferences.getInt(KEY_POSITION, 0);

        return this;
    }

    public String getText() {
        return mText;
    }

    public MojeUstawienia setText(String mText) {
        this.mText = mText;
        return this;
    }

    public Boolean getCheck() {
        return mCheck;
    }

    public MojeUstawienia setCheck(Boolean mCheck) {
        this.mCheck = mCheck;
        return this;
    }

    public Integer getPosition() {
        return mPosition;
    }

    public MojeUstawienia setPosition(Integer mPosition) {
        this.mPosition = mPosition;
        return this;
    }

    public void save() {
        // Przepisanie wartości z pól klasy do SharedPreferences
        mPreferences.edit()
                .putString(KEY_TEXT, mText)
                .putBoolean(KEY_CHECK, mCheck)
                .putInt(KEY_POSITION, mPosition)
                .commit();  // Trzeba pamiętać o zapisaniu tych wartości po edycji !!!
    }
}
