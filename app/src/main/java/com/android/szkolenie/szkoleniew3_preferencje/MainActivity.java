package com.android.szkolenie.szkoleniew3_preferencje;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;


public class MainActivity extends ActionBarActivity {

    private EditText mText;
    private CheckBox mCheckBox;
    private RadioGroup mRadioGroup;
    private RadioButton mRadio1, mRadio2;
    private Button mZapisz;

    private MojeUstawienia mUstawienia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mText = (EditText) findViewById(R.id.ustawienie_text);
        mCheckBox = (CheckBox) findViewById(R.id.ustawienie_checkbox);
        mRadioGroup = (RadioGroup) findViewById(R.id.ustawienie_radio);
        mRadio1 = (RadioButton) findViewById(R.id.ustawienie_radio1);
        mRadio2 = (RadioButton) findViewById(R.id.ustawienie_radio2);
        mZapisz = (Button) findViewById(R.id.btn_zapisz);

        // Utworzenie klasy zarządzania ustawieniami
        mUstawienia = new MojeUstawienia(this);

        // Zadanie 1 - dopisz do przycisku Zapisz logikę odczytującą dane z formularza i zapisującą
        // do preferencji w klasie MojeUstawienia.
        // Zadanie BEGIN
        mZapisz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mUstawienia.setText(mText.getText().toString())
                        .setCheck(mCheckBox.isChecked())
                        .setPosition(mRadioGroup.getCheckedRadioButtonId())
                        .save();
            }
        });
        // Zadanie END

        // Jeżeli przekręcamy ekran to niech odtworzy sie to co było wpisane
        // Przy pierwszym uruchomieniu tego ekranu niech wczyta dane z preferencji

        readPreferences();
    }

    // Załadowanie danych z klasy MojeUstawienia do widoku formularza
    protected void readPreferences() {
        mText.setText(mUstawienia.getText());
        mCheckBox.setChecked(mUstawienia.getCheck());
        mRadioGroup.check(mUstawienia.getPosition());
    }
}
